import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function Register(){

	const {user} = useContext(UserContext);
	
	const history = useNavigate();

	const[firstName, setFirstName] = useState('');
	const[lastName, setLastName] = useState('');
	const[mobileNo, setmobileNo] = useState('');
	const [email, setEmail] = useState ('');
	const [password, setPassword]= useState('');
	// const [password2, setPassword2]= useState('');
	const [isActive, setIsActive] = useState(false);


	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	// simulation of user registration

	function registerUser (e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data===true){
				Swal.fire({
					title: "Email Already Exists",
					icon: "error",
					text: "Please provide another email"
				})
			} else {

				fetch('http://localhost:4000/users/register', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);
					if(data === true){

						setFirstName('');
						setLastName('');
						setmobileNo('');
						setEmail('');
						setPassword('');

						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "Welcom to Zuitt!"
						})

						history("/login")

					} else {

						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again"
						})						
					}
				})
			}
		})

		//clear input fields
/*		setFirstName('');
		setLastName('');
		setmobileNo('');
		setEmail('');
		setPassword('');*/
		// setPassword2('');

		// alert('Thank you for registering!');
	}

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNo !== '' && email !== '' && password !== '')){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNo, email, password])

	return(
		(user.id !== null) ?

		<Navigate to= "/courses"/>

		:

		<Form onSubmit={registerUser}>
			<h1>Register</h1>
			<Form.Group controlId="firstName" className="my-2">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type = "text"
					placeholder = "Input your First Name here"
					value = {firstName}
					onChange = { e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastName" className="my-2">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type = "text"
					placeholder = "Input your Last Name here"
					value = {lastName}
					onChange = { e => setLastName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo" className="my-2">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type = "text"
					placeholder = "Input your Mobile Number here"
					value = {mobileNo}
					onChange = { e => setmobileNo(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="userEmail" className="my-2">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type = "email"
					placeholder = "Enter your email here"
					value = {email}
					onChange = { e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We will never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password" className="my-2">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Input your password here"
					value = {password}
					onChange = { e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

{/*			<Form.Group controlId="password2" className="my-2">
				<Form.Label>Confirm Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Input your password again"
					value = {password2}
					onChange = { e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>*/}
			
			{/*conditionally redering submit button based on isActive state*/}
			
			{ isActive ?
				<Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-3">
				Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-3" disabled>
				Submit
				</Button>
			}
		</Form>
	)
}