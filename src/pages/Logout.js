import{useContext, useEffect} from 'react'
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'

export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext);

	// clears the localStorage of the user's information
	unsetUser();

	useEffect(() => {
		setUser({id:null})
	}, [])

	//localStorage.clear()

	alert('You are now logged out.');
	
	return(
		<Navigate to='/login'/>
	)
}