import Banner from '../components/Banner'

export default function Error(){
	
	const data = {
		title: "404: Page Not Found",
		content: "Make sure the address is correct and the page hasn't moved.",
		destination: "/",
		label: "Back to Homepage"
	}

	return(

		<Banner data={data}/>

	)
}
