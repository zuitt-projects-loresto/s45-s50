import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function Login(props){

	// allows us to consume the User context object and its properties to use for user validation
	const {user, setUser} = useContext(UserContext);
	console.log(user)

	const [email, setEmail] = useState ('');
	const [password, setPassword]= useState('');
	const [isActive, setIsActive] = useState(false);

	const Swal = require('sweetalert2')

	function authenticate (e) {
		e.preventDefault();


		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== "undefined"){

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
				  title: 'Login Successful',
				  text: 'Welcome to Zuitt!',
				  icon: 'success',
				  confirmButtonText: 'OK'
				})

			}else{
				Swal.fire({
				  title: 'Authentication Failed',
				  text: 'Check the email or password',
				  icon: 'error',
				  confirmButtonText: 'Try again'
				})
			}
		})

		// set the email of the authenticated user in the local storage

		// Syntax: localStorage.setItem("key", value)
		// localStorage.setItem("email", email)

		// to access the user information, it can be done using the localStorage, this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout

		// when state change components are rerendered and the AppNavBar component will be updated based on the user credentials
		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		setEmail('');
		setPassword('');

		const retrieveUserDetails = (token) => {
			fetch('http://localhost:4000/users/details', {
				method: "POST",
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}

		// alert('You are now logged in.');
	}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(
		(user.id !== null) ?

		<Navigate to= "/courses"/>

		:

		<Form onSubmit={e => authenticate(e)}>
			<h1>Login</h1>
			<Form.Group controlId="userEmailLogin" className="my-2">
			<Form.Label>Email Address</Form.Label>
			<Form.Control
				type = "email"
				placeholder = "Enter your registered email here"
				value = {email}
				onChange = { e => setEmail(e.target.value)}
				required
			/>
			</Form.Group>

			<Form.Group controlId="password"  className="my-2">
			<Form.Label>Password</Form.Label>
			<Form.Control
				type = "password"
				placeholder = "Enter your password here"
				value = {password}
				onChange = { e => setPassword(e.target.value)}
				required
			/>
			</Form.Group>
			
			{ isActive ?
				<Button variant="success" type="submit" id="loginBtn" className="mt-3 mb-3">
				Login
				</Button>
				:
				<Button variant="success" type="submit" id="loginBtn" className="mt-3 mb-3" disabled>
				Login
				</Button>
			}
		</Form>
	)
}