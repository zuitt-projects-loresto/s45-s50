import {useState, useEffect} from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}){
	
	console.log(courseProp); // result coursesData[0]
	console.log(typeof courseProp); // result: object


	//const [count, setCount] = useState(0);
	//syntax: const [getter, setter] = useState(initialValueOfGetter)

	/*const [seat, setSeat] = useState(30);
	const [isOpen, setIsOpen] = useState(false);

	function enroll(){
		if(seat > 0){
			setCount(count + 1)
			setSeat(seat - 1)
			console.log('Enrollees' + count)
		} else {
			alert('No more seats left. Try to enroll in other courses or wait for an opening.')
		}
	}*/

	/*useEffect(() => {
		if(seat === 0){
			setIsOpen(false)
			alert('No more seats left.')
		}
	}, [seat])*/
	// syntax: useEffect (() => {}, [optionalParameter])

	const {name, description, price, _id} = courseProp

	return(
		<Row className="mt-3 mb-3">

			<Col xs={12} md={12}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>Php {price}</Card.Text>
						<Button variant="primary" as={Link} to={`/courses/${_id}`}>See Details</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}