import React from 'react'
import {Row, Col, Button} from 'react-bootstrap'

export default function Banner() {
	return(
		<Row>
			<Col className = "p-5">
				<h1>Page Not Found</h1>
				<p> Make sure the address is correct and the page hasn't moved. Go back to <a href="/">Home Page</a></p>
			</Col>
		</Row>
	)
}